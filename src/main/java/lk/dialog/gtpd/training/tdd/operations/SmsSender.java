package lk.dialog.gtpd.training.tdd.operations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SmsSender {

    private Logger logger = LogManager.getLogger(getClass());


    private NumberFormatter numberFormatter;
    private SmsCreator smsCreator;

    public SmsSender() {
        numberFormatter = new NumberFormatter();
        smsCreator = new SmsCreator();
    }

    public void sendSms(String number) {
        logger.info("-------\nTo: {}\n{}====",
                numberFormatter.getCompatibleNumberFormat(number),
                smsCreator.getSmsContent());
    }
}
